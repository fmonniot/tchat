package eu.monniot.chat.server.algorithms;

/**
 * Used to create the map of identities en the server state
 */
public interface IdentityMessage {
    /**
     * @return the identity of this message sender
     */
    int getSender();
}
