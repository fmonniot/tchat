package eu.monniot.chat.server;

import eu.monniot.chat.commons.DelayedMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Used in debug mode to delay sending message
 */
public class DelayedMessageDispatcher implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    private final ChatServerMain server;

    private boolean run = true;

    DelayedMessageDispatcher(ChatServerMain serverMain) {
        server = serverMain;
    }

    @Override
    public void run() {
        List<DelayedMessage> delayedMessages = server.delayedMessages;

        while (run) {
            if (delayedMessages.size() > 0) {
                synchronized (server.delayedMessages) {
                    logger.debug("Delay, start sending message (from " + (delayedMessages) + " )");

                    DelayedMessage m = delayedMessages.get(0);
                    logger.trace("message " + m + " chosen.");
                    if (m != null) {
                        try {
                            server.forward(m.key, m.messType, m.identity, m.seqNumber, m.msgSerializable);
                            delayedMessages.remove(0);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public void stop() {
        run = false;
    }
}
