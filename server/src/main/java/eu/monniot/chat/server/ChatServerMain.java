package eu.monniot.chat.server;

import eu.monniot.chat.client.algorithms.ListOfAlgorithmsClient;
import eu.monniot.chat.client.algorithms.chat.AlgorithmChat;
import eu.monniot.chat.client.algorithms.chat.ExitMessage;
import eu.monniot.chat.commons.AlgorithmActionInvocationException;
import eu.monniot.chat.commons.DelayedMessage;
import eu.monniot.chat.commons.FullDuplexMsgWorker;
import eu.monniot.chat.commons.ReadMessageStatus;
import eu.monniot.chat.server.algorithms.IdentityMessage;
import eu.monniot.chat.server.algorithms.ListOfAlgorithmsServer;
import eu.monniot.chat.server.algorithms.termination.AlgorithmTerminationDetectionActions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.*;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


/**
 * This class defines the main of the chat server. It configures the server,
 * connects to existing chat servers, waits for connections from other chat
 * servers and from chat clients, and forwards chat messages received from chat
 * clients to other 'local' chat clients and to the other chat servers.
 * <p/>
 * The chat servers can be organised into a network topology forming cycles
 * since the method <tt>forward</tt> is only called when the message to forward
 * has not already been received and forwarded.
 *
 * @author christian bac
 * @author Denis Conan
 */
public class ChatServerMain {

    private static final Logger logger = LogManager.getLogger();

    /**
     * selection keys of the server message workers.
     */
    private final HashMap<SelectionKey, FullDuplexMsgWorker> allServerWorkers;

    /**
     * selection keys of the client message workers.
     */
    private final HashMap<SelectionKey, FullDuplexMsgWorker> allClientWorkers;

    /**
     * set to true for displaying debug messages on the console.
     */
    private boolean debugState = true;

    /**
     * server's state for the distributed eu.monniot.chat.server.eu.monniot.chat.client.algorithms implemented in the server.
     */
    private final ChatServerState serverState;

    /**
     * selector object for the main loop waiting for connections and messages.
     */
    private Selector selector;

    /**
     * selection keys for accepting connections from clients and servers.
     */
    private SelectionKey acceptClientKey, acceptServerKey;

    /**
     * server socket channel for accepting client and server connections.
     */
    private ServerSocketChannel listenChanClient, listenChanServer;

    /**
     * each client is assigned an identity in the form of an integer.
     */
    private int clientNumber = 0;

    final List<DelayedMessage> delayedMessages;

    private ChatServerMain(final int identity, final int port, final boolean debug) throws IOException {
        debugState = debug;
        allClientWorkers = new HashMap<>();
        allServerWorkers = new HashMap<>();

        serverState = new ChatServerState();
        serverState.clientSeqNumbers = new HashMap<>();
        serverState.server = this;
        serverState.identity = identity;
        InetSocketAddress rcvAddress;
        ServerSocket listenSocket;

        // creates the selector and the listeners
        selector = Selector.open();
        listenChanClient = ServerSocketChannel.open();
        listenSocket = listenChanClient.socket();
        rcvAddress = new InetSocketAddress(port);
        listenSocket.setReuseAddress(true);
        listenSocket.bind(rcvAddress);
        listenChanClient.configureBlocking(false);

        listenChanServer = ServerSocketChannel.open();
        listenSocket = listenChanServer.socket();
        rcvAddress = new InetSocketAddress(port + 100);
        listenSocket.setReuseAddress(true);
        listenSocket.bind(rcvAddress);
        listenChanServer.configureBlocking(false);

        // register selection keys
        acceptClientKey = listenChanClient.register(selector,
                SelectionKey.OP_ACCEPT);
        acceptServerKey = listenChanServer.register(selector,
                SelectionKey.OP_ACCEPT);

        delayedMessages = Collections.synchronizedList(new ArrayList<DelayedMessage>());

        final Thread commandThread = new Thread(new ChatServerThreadReadCommand(serverState));
        commandThread.setName("ReadCommand");
        commandThread.start();

        final Thread dispatcherThread = new Thread(new DelayedMessageDispatcher(this));
        dispatcherThread.setName("DelayedMessageDispatcher");
        dispatcherThread.start();
    }

    /**
     * main method that creates an instance of the server and then connects the
     * server to other servers, whose connection data are given provided as
     * arguments of the main.
     *
     * @param args java command arguments.
     */
    public static void main(final String[] args) {
        checkArgument((args.length >= 1) && ((args.length % 2) != 0),
                "usage: java ChatSerialMultiServerMain <server number> <list of pairs hostname servernumber>");
        final boolean debug = System.getenv().containsKey("DEBUG");

        ChatServerMain chatMulti;
        try {
            chatMulti = new ChatServerMain(Integer.parseInt(args[0]), 2050 + Integer.parseInt(args[0]), debug);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        for (int i = 1; i < args.length; i = i + 2) {
            try {
                chatMulti.addServer(args[i], (2050 + Integer.parseInt(args[i + 1]) + 100));
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }

        chatMulti.loop();
    }

    /**
     * connect socket, create MsgWorker, and register selection key of the
     * remote server. This method is called when connecting to a remote server.
     * Connection data are provided as arguments to the main.
     *
     * @param host remote host's name.
     * @param port remote port's number.
     *
     * @throws IOException
     */
    void addServer(final String host, final int port) throws IOException {
        logger.info("Opening connection with server on host " + host + " on port " + port);

        InetAddress dstAddress = InetAddress.getByName(host);
        SocketChannel rwChan = SocketChannel.open();
        Socket rwSock = rwChan.socket();

        // obtain the IP address of the target host
        InetSocketAddress rcvAddress = new InetSocketAddress(dstAddress, port);

        // connect sending socket to remote port
        rwSock.connect(rcvAddress);
        FullDuplexMsgWorker worker = new FullDuplexMsgWorker(rwChan);
        worker.configureNonBlocking();
        SelectionKey serverKey = rwChan.register(selector, SelectionKey.OP_READ);
        allServerWorkers.put(serverKey, worker);
        // servers.put(serverKey, rwChan);
        logger.info("  allServerWorkers.size() = " + allServerWorkers.size());
    }

    /**
     * accept connection (socket level), create MsgWorker, and register
     * selection key of the remote server. This method is called when accepting
     * a connection from a remote server.
     *
     * @param sc server socket channel.
     *
     * @throws IOException
     */
    void acceptNewServer(final ServerSocketChannel sc) throws IOException {
        SocketChannel rwChan = sc.accept();
        checkNotNull(rwChan);
        try {
            FullDuplexMsgWorker worker = new FullDuplexMsgWorker(rwChan);
            worker.configureNonBlocking();
            SelectionKey newKey = rwChan.register(selector, SelectionKey.OP_READ);

            allServerWorkers.put(newKey, worker);
            serverState.nbNeighbours++;
            logger.info("  allServerWorkers.size() = " + allServerWorkers.size());
        } catch (ClosedChannelException e) {
            e.printStackTrace();
        }
    }

    /**
     * accept connection (socket level), create MsgWorker, and register
     * selection key of the local client. This method is called when accepting a
     * connection from a local client.
     *
     * @param sc server socket channel.
     *
     * @throws IOException
     */
    void acceptNewClient(final ServerSocketChannel sc) throws IOException {
        SocketChannel rwChan = sc.accept();

        checkNotNull(rwChan);
        try {
            FullDuplexMsgWorker worker = new FullDuplexMsgWorker(rwChan);
            worker.configureNonBlocking();
            SelectionKey newKey = rwChan.register(selector, SelectionKey.OP_READ);

            allClientWorkers.put(newKey, worker);
            worker.sendMsg(0, serverState.identity, serverState.seqNumber, serverState.identity * 100 + clientNumber);
            clientNumber++;
        } catch (ClosedChannelException e) {
            e.printStackTrace();
        }
    }

    /**
     * send a message to all the remote servers / neighbours connected to this
     * server. This is a utility method for implementing distributed eu.monniot.chat.server.eu.monniot.chat.client.algorithms
     * in the servers' state machine: use this method when this server needs
     * sending messages to its neighbours.
     *
     * @param type      message's type.
     * @param identity  sender's identity, that is the identity of this server.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    public void sendToAllServers(final int type, final int identity,
                                 final int seqNumber, Serializable s) throws IOException {
        serverState.seqNumber++;
        // send to all the servers, thus first argument is null
        forwardServers(null, type, identity, seqNumber, s);
    }

    /**
     * send a message to a particular remote server / neighbour. This is a
     * utility method for implementing distributed eu.monniot.chat.server.eu.monniot.chat.client.algorithms in the servers'
     * state machine: use this method when this server needs sending messages to
     * a neighbour.
     *
     * @param targetKey selection key of the neighbour.
     * @param type      message's type.
     * @param identity  sender's identity, that is the identity of this server.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    public void sendToAServer(final SelectionKey targetKey, final int type,
                              final int identity, final int seqNumber, Serializable s)
            throws IOException {
        serverState.seqNumber++;
        FullDuplexMsgWorker sendWorker = allServerWorkers.get(targetKey);
        if (sendWorker == null) {
            logger.warn("Bad receiver for server key " + targetKey);
        } else {
            sendWorker.sendMsg(type, identity, seqNumber, s);
        }
        if (debugState) {
            logger.debug("Send message to a given server");
        }
    }

    /**
     * send a message to all the remote servers / neighbours connected to this
     * server, except one. This is a utility method for implementing distributed
     * eu.monniot.chat.server.eu.monniot.chat.client.algorithms in the servers' state machine: use this method when this
     * server needs sending messages to its neighbours, except one.
     *
     * @param type      message's type.
     * @param identity  sender's identity, that is the identity of this server.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    public void sendToAllServersExceptOne(final SelectionKey exceptKey, final int type, final int identity,
                                          final int seqNumber, Serializable s) throws IOException {
        serverState.seqNumber++;
        forwardServers(exceptKey, type, identity, seqNumber, s);
    }

    /**
     * forward a message to all the clients and the servers, except the entity
     * (client or server) from which the message has just been received.
     *
     * @param exceptKey selection key to exclude from the set of target connections,
     *                  e.g., selection key of the entity from which the message has
     *                  been received.
     * @param type      message's type.
     * @param identity  sender's identity.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    void forward(final SelectionKey exceptKey, final int type, final int identity, final int seqNumber, Serializable s)
            throws IOException {
        logger.info("forward message "+s);
        forwardServers(exceptKey, type, identity, seqNumber, s);
        forwardClients(exceptKey, type, identity, seqNumber, s);
    }

    /**
     * forward a message to all the servers, except the server from which the
     * message has just been received.
     *
     * @param exceptKey selection key to exclude from the set of target connections,
     *                  e.g., selection key of the entity from which the message has
     *                  been received.
     * @param type      message's type.
     * @param identity  sender's identity.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    void forwardServers(final SelectionKey exceptKey, final int type,
                        final int identity, final int seqNumber, Serializable s)
            throws IOException {
        int nbServers = 0;
        for (SelectionKey target : allServerWorkers.keySet()) {
            if (target == exceptKey) {
                logger.trace("do not send to server because (target == exceptKey)");
                continue;
            }
            FullDuplexMsgWorker worker = allServerWorkers.get(target);
            if (worker == null) {
                logger.warn("Bad worker for server key " + target);
            } else {
                worker.sendMsg(type, identity, seqNumber, s);
                nbServers++;
            }
        }
        logger.debug("Send message to " + nbServers + " server end points");
    }

    /**
     * forward a message to all the clients, except the client from which the
     * message has just been received.
     *
     * @param targetKey selection key to exclude from the set of target connections,
     *                  e.g., selection key of the entity from which the message has
     *                  been received.
     * @param type      message's type.
     * @param identity  sender's identity.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    void forwardClients(final SelectionKey targetKey, final int type,
                        final int identity, final int seqNumber, Serializable s)
            throws IOException {
        int nbClients = 0;

        for (SelectionKey target : allClientWorkers.keySet()) {
            if (target == targetKey) {
                logger.trace("do not send to client because (target == exceptKey)");
                continue;
            }
            FullDuplexMsgWorker clientWorker = allClientWorkers.get(target);
            if (clientWorker == null) {
                logger.info("Bad receiver for key " + target);
            } else {
                logger.trace("Send message " + s + " to " + clientWorker.getChannel().getRemoteAddress().toString());

                clientWorker.sendMsg(type, identity, seqNumber, s);
                nbClients++;
            }
        }
        logger.debug("Send message to " + nbClients + " client end points");
    }

    /**
     * Delay the message by storing it and sending it randomly each seconds.
     *
     * This is a utility method used for introducing randomisation into the transmission of
     * messages through the distributed system. Use this method to augment
     * ordering problems when testing FIFO or causal broadcast algorithms.
     *
     * @see #forward(java.nio.channels.SelectionKey, int, int, int, java.io.Serializable)
     * @param exceptKey selection key to exclude from the set of target connections,
     *                  e.g., selection key of the entity from which the message has
     *                  been received.
     * @param type      message's type.
     * @param identity  sender's identity.
     * @param seqNumber message's sequence number.
     * @param s         message as an serializable object.
     *
     * @throws IOException
     */
    void forwardOrDelay(final SelectionKey exceptKey, final int type,
                        final int identity, final int seqNumber, Serializable s)
            throws IOException {
        DelayedMessage m = new DelayedMessage(exceptKey, type, identity, seqNumber, s);
        logger.trace("Delay message ", m);
        synchronized (delayedMessages) {
            delayedMessages.add(m);
        }
    }

    /**
     * infinite loop organised around the call to select.
     */
    void loop() {
        logger.info("Entering the infinite loop");
        logger.info("listenChanClient ok on port " + listenChanClient.socket().getLocalPort());
        logger.info("listenChanServer ok on port " + listenChanServer.socket().getLocalPort());
        serverState.nbNeighbours = allServerWorkers.size();
        while (true) {
            AlgorithmTerminationDetectionActions.beforeReceive(serverState);
            try {
                selector.select();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            Set<SelectionKey> readyKeys = selector.selectedKeys();
            Iterator<SelectionKey> readyIterator = readyKeys.iterator();
            while (readyIterator.hasNext()) {
                SelectionKey key = readyIterator.next();
                readyIterator.remove();

                if (key.isAcceptable()) {
                    try {
                        if (key.equals(acceptServerKey)) {
                            acceptNewServer(listenChanServer);
                            logger.info("allServerWorkers.size() = " + allServerWorkers.size());
                        } else if (key.equals(acceptClientKey)) {
                            acceptNewClient(listenChanClient);
                            logger.info("allClientWorkers.size() = " + allClientWorkers.size());
                        } else { // error
                            logger.error("unknown accept");
                            return;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (key.isReadable()) {
                    serverState.currKey = key;

                    FullDuplexMsgWorker readWorker = allServerWorkers.get(key);
                    // message comes from another server
                    if (readWorker != null) {
                        try {
                            ReadMessageStatus status;
                            status = readWorker.readMessage();

                            // remote end point has been closed
                            if (status == ReadMessageStatus.ChannelClosed) {
                                readWorker.close();
                                allServerWorkers.remove(key);
                                logger.debug("Closing a channel");
                                logger.debug("allServerWorkers.size() = " + allServerWorkers.size());
                            }

                            // fully received a message
                            if (status == ReadMessageStatus.ReadDataCompleted) {
                                int messType = readWorker.getInType();
                                // place where we get the data from the message
                                Serializable msg = readWorker.getData();
                                logger.info("Message received from server: " + readWorker.getInSeqNumber()
                                        + ", " + msg);

                                if (messType < ListOfAlgorithmsClient.CHAT_CLIENT_START_INDEX) {
                                    // server message to execute

                                    // map the identity of the server with its SelectionKey
                                    if(msg instanceof IdentityMessage) {
                                        int sender = ((IdentityMessage) msg).getSender();
                                        if(!serverState.identitiesMap.containsKey(sender)) {
                                            logger.debug("Map node " + sender + " with key " + key);
                                            serverState.identitiesMap.put(sender, key);
                                        }
                                    }

                                    // message for server's state machine
                                    logger.info("Going to execute action for message type #" + messType
                                            + " on content " + msg);
                                    ListOfAlgorithmsServer.execute(serverState, messType, msg);
                                } else {
                                    // client message to forward
                                    int identity = readWorker.getInIdentity();
                                    int seqNumber = readWorker.getInSeqNumber();

                                    if (serverState.clientSeqNumbers.get(identity) == null) {
                                        logger.debug("New message from client to forward");

                                        serverState.clientSeqNumbers.put(identity, seqNumber);
                                        if (debugState) {
                                            forwardOrDelay(key, messType, identity, seqNumber, msg);
                                        } else {
                                            forward(key, messType, identity, seqNumber, msg);
                                        }
                                    } else {
                                        if (seqNumber > serverState.clientSeqNumbers.get(identity)) {
                                            logger.debug("Message not already forwarded");

                                            serverState.clientSeqNumbers.put(identity, seqNumber);
                                            if (debugState) {
                                                forwardOrDelay(key, messType, identity, seqNumber, msg);
                                            } else {
                                                forward(key, messType, identity, seqNumber, msg);
                                            }
                                        } else  {
                                            logger.debug("Already forwarded message NOT to forward");
                                        }
                                    }
                                }
                            }
                        } catch (IOException | AlgorithmActionInvocationException e) {
                            e.printStackTrace();
                        }
                    }
                    // end message comes from another server

                    FullDuplexMsgWorker clientWorker = allClientWorkers.get(key);
                    // message comes from a client
                    if (clientWorker != null) {
                        try {
                            ReadMessageStatus status;
                            status = clientWorker.readMessage();

                            // remote end point has been closed
                            if (status == ReadMessageStatus.ChannelClosed) {
                                clientWorker.close();
                                allClientWorkers.remove(key);
                                logger.debug("Closing a channel");
                                logger.debug("allClientWorkers.size() = " + allClientWorkers.size());
                            }

                            // fully received a message
                            if (status == ReadMessageStatus.ReadDataCompleted) {
                                int messType = clientWorker.getInType();
                                // place where we get the data from the message
                                Serializable msg = clientWorker.getData();
                                logger.debug("Message received from client: " + msg + " ");

                                // client want to be disconnected
                                if(msg instanceof ExitMessage) {
                                    clientWorker.sendMsg(AlgorithmChat.EXIT_MESSAGE.getActionIndex(),
                                            serverState.getIdentity(), 0, msg);
                                    clientWorker.close();
                                    allClientWorkers.remove(key);
                                    clientNumber--;
                                    continue;
                                }

                                // Sending message from client to others
                                int identity = clientWorker.getInIdentity();
                                int seqNumber = serverState.seqNumber++;
                                serverState.clientSeqNumbers.put(identity, seqNumber);
                                if (debugState) {
                                    forwardOrDelay(key, messType, identity, seqNumber, msg);
                                } else {
                                    forward(key, messType, identity, seqNumber, msg);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    // end message comes from a client
                }
            }
        }
    }
}
