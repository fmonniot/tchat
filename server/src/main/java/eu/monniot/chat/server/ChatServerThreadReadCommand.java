package eu.monniot.chat.server;

import eu.monniot.chat.server.algorithms.election.AlgorithmElection;
import eu.monniot.chat.server.algorithms.election.TokenMessage;
import eu.monniot.chat.server.algorithms.mutualExclusion.AlgorithmExclusionActions;
import eu.monniot.chat.server.algorithms.termination.AlgorithmTerminationDetectionActions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class is a thread reading command lines to control the server.
 * <p/>
 * NB: be careful! When manipulating the server's state, acquire the semaphore
 * before and release it after.
 *
 * @author Denis Conan
 */
class ChatServerThreadReadCommand implements Runnable {

    private static Logger logger = LogManager.getLogger();

    private final ChatServerState serverState;

    public ChatServerThreadReadCommand(ChatServerState state) {
        serverState = state;
    }

    public void run() {
        String s;
        BufferedReader bufin = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                s = bufin.readLine();
                if (s == null) {
                    System.out.println("Error null string, leaving");
                    return;
                }
            } catch (IOException ie) {
                ie.printStackTrace();
                return;
            }

            // add here the treatment of the different server commands
            System.out.println("Server command read: " + s);
            switch (s){
                case "elect":
                    if(serverState.getWin() < 0) {
                        try {
                            serverState.getSemaphore().acquire();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        serverState.setCaw(serverState.getIdentity());
                        try {
                            serverState.server.sendToAllServers(
                                    AlgorithmElection.RECEIVE_TOKEN.getActionIndex(),
                                    serverState.getIdentity(), serverState.getSeqNumber(),
                                    new TokenMessage(serverState.getIdentity(), serverState.getIdentity())
                            );
                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            serverState.getSemaphore().release();
                        }
                    }
                    break;
                case "save":
                    AlgorithmExclusionActions.enterCriticalSection(serverState, new Runnable() {
                        @Override
                        public void run() {
                            try {
                                logger.debug("Begin save action.");
                                Thread.sleep(5000);
                                logger.debug("End save action");
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
                case "exit":
                    AlgorithmTerminationDetectionActions.init(serverState, new Runnable() {
                        @Override
                        public void run() {
                            logger.debug("run custom action");
                        }
                    });
                    break;
            }
        }
    }
}
