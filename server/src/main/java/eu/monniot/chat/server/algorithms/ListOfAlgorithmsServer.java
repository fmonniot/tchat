package eu.monniot.chat.server.algorithms;

import eu.monniot.chat.server.algorithms.election.AlgorithmElection;
import eu.monniot.chat.commons.*;
import eu.monniot.chat.server.algorithms.mutualExclusion.AlgorithmExclusion;
import eu.monniot.chat.server.algorithms.termination.AlgorithmTerminationDetection;

import java.util.Map;

/**
 * This Enumeration type declares the eu.monniot.chat.server, eu.monniot.chat.client.algorithms of the server's state machine.
 * For now, there is only one algorithm: the algorithm for the election.
 *
 * @author Denis Conan
 */
public enum ListOfAlgorithmsServer implements ListOfAlgorithms{
    /**
     * message types exchanged between servers for the election algorithm.
     */
    ALGORITHM_ELECTION(AlgorithmElection.mapOfActions),
    ALGORITHM_EXCLUSION(AlgorithmExclusion.mapOfActions),
    ALGORITHM_TERMINATION(AlgorithmTerminationDetection.mapOfActions);

    /**
     * index of the first message type of the first algorithm of the server.
     */
    public final static int CHAT_SERVER_START_INDEX = 0;

    private final Map<Integer, ? extends AlgorithmActionInterface> mapOfActions;

    /**
     * constructor of this algorithm object.
     *
     * @param map collection of actions of this algorithm.
     */
    private ListOfAlgorithmsServer(Map<Integer, ? extends AlgorithmActionInterface> map) {
        mapOfActions = map;
    }

    @Override
    public Map<Integer, ? extends AlgorithmActionInterface> getActions() {
        return mapOfActions;
    }

    /**
     * Try to execute the action with a server algorithm
     *
     * @see ListOfAlgorithmsDelegate#execute(State, int, Object, ListOfAlgorithms[])
     */
    public static void execute(final State state, final int actionIndex, final Object content)
            throws AlgorithmActionInvocationException {
        ListOfAlgorithmsDelegate.execute(state, actionIndex, content, ListOfAlgorithmsServer.values());
    }
}
