package eu.monniot.chat.commons;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VectorClockTest {

    private VectorClock vc1;

    @Before
    public void setUp() throws Exception {
        vc1 = new VectorClock();
    }

    @After
    public void tearDown() throws Exception {
        vc1 = null;
    }

    @Test
    public void testGetEntry() throws Exception {
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        Assert.assertNotNull(vc1.getEntry(1));
    }

    @Test
    public void testGetSetEntry() throws Exception {
        vc1.setEntry(0, 0);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        vc1.setEntry(0, 1);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        vc1.setEntry(1, 10);
        Assert.assertNotNull(vc1.getEntry(1));
        Assert.assertEquals(Integer.valueOf(10), vc1.getEntry(1));
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertNotSame(10, vc1.getEntry(0));
    }

    @Test
    public void testIncrement() throws Exception {
        vc1.setEntry(0, 0);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        vc1.incrementEntry(0);
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        vc1.incrementEntry(1);
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(1));
        vc1.incrementEntry(1);
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(2), vc1.getEntry(1));
        vc1.incrementEntry(2);
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(2), vc1.getEntry(1));
        Assert.assertEquals(Integer.valueOf(1), vc1.getEntry(2));
    }

    @Test
    public void testClone() throws Exception {
        VectorClock vc2 = (VectorClock) vc1.clone();
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertNotNull(vc2.getEntry(0));
        vc2.setEntry(1, 1);
        Assert.assertEquals(Integer.valueOf(1), vc2.getEntry(1));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(1));
        VectorClock vc3 = (VectorClock) vc2.clone();
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertNotNull(vc2.getEntry(0));
        Assert.assertNotNull(vc2.getEntry(1));
        Assert.assertNotNull(vc3.getEntry(0));
        Assert.assertNotNull(vc3.getEntry(1));
        Assert.assertEquals(Integer.valueOf(0), vc2.getEntry(0));
        Assert.assertEquals(Integer.valueOf(1), vc2.getEntry(1));
    }

    @Test
    public void testMax() throws Exception {
        vc1.max(null);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        VectorClock vc2 = (VectorClock) vc1.clone();
        vc1.max(vc2);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        vc2.setEntry(1, 10);
        vc1.max(vc2);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        Assert.assertEquals(Integer.valueOf(10), vc1.getEntry(1));
        vc1.setEntry(2, 10);
        vc1.max(vc2);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        Assert.assertEquals(Integer.valueOf(10), vc1.getEntry(1));
        Assert.assertNotNull(vc1.getEntry(2));
        Assert.assertEquals(Integer.valueOf(10), vc1.getEntry(2));
        vc2.setEntry(1, 100);
        vc1.max(vc2);
        Assert.assertNotNull(vc1.getEntry(0));
        Assert.assertEquals(Integer.valueOf(0), vc1.getEntry(0));
        Assert.assertNotNull(vc1.getEntry(1));
        Assert.assertEquals(Integer.valueOf(100), vc1.getEntry(1));
        Assert.assertNotNull(vc1.getEntry(2));
        Assert.assertEquals(Integer.valueOf(10), vc1.getEntry(2));
    }

    @Test
    public void testIsPrecededByAndFIFO() throws Exception {
        VectorClock vc2 = new VectorClock();
        Assert.assertFalse(vc1.isPrecededByAndFIFO(vc2, 0));
        vc2.incrementEntry(0);
        Assert.assertTrue(vc1.isPrecededByAndFIFO(vc2, 0));
        vc1.incrementEntry(0);
        Assert.assertFalse(vc1.isPrecededByAndFIFO(vc2, 0));
        vc2.incrementEntry(0);
        Assert.assertTrue(vc1.isPrecededByAndFIFO(vc2, 0));
        vc1.incrementEntry(1);
        Assert.assertTrue(vc1.isPrecededByAndFIFO(vc2, 0));
    }

}
