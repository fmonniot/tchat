package eu.monniot.chat.client.algorithms.chat;

import eu.monniot.chat.client.ChatClientState;
import eu.monniot.chat.client.algorithms.ListOfAlgorithmsClient;
import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.commons.AlgorithmActionInterface;
import eu.monniot.chat.commons.AlgorithmActionInvocationException;
import eu.monniot.chat.commons.State;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This Enumeration type declares the algorithm of the chat part of the client's
 * state machine. Only one message content type can be received.
 *
 * @author Denis Conan
 */
public enum AlgorithmChat implements AlgorithmActionInterface {

    /**
     * message type to chat between clients.
     */
    CHAT_MESSAGE() {
        public Object execute(final State state, final AbstractMessage msg)
                throws AlgorithmActionInvocationException {

            if (!(state instanceof ChatClientState)) throw new IllegalArgumentException(
                    "Try executing action with state of wrong type (" + state.getClass().getName() +
                            " instead of eu.monniot.chat.client.ChatClientState"
            );

            if (!(msg instanceof ChatMessage)) throw new AlgorithmActionInvocationException(
                    "Error when executing action: not right message type (" + msg.getClass().getName() + ")"
            );

            return AlgorithmChatActions.receiveChatMessage((ChatClientState) state, (ChatMessage) msg);
        }
    },
    EXIT_MESSAGE() {
        @Override
        public Object execute(State state, AbstractMessage msg) throws AlgorithmActionInvocationException {
            if (!(state instanceof ChatClientState)) throw new IllegalArgumentException(
                    "Try executing action with state of wrong type (" + state.getClass().getName() +
                            " instead of eu.monniot.chat.client.ChatClientState"
            );

            if (!(msg instanceof ExitMessage)) throw new AlgorithmActionInvocationException(
                    "Error when executing action: not right message type (" + msg.getClass().getName() + ")"
            );

            return AlgorithmChatActions.receiveExitMessage((ExitMessage) msg);
        }
    };

    /**
     * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
     */
    public final static Map<Integer, AlgorithmChat> mapOfActions;

    /**
     * collection of the actions of this algorithm. The Key is the index of the
     * corresponding message type. This collection is kept private to avoid
     * modifications.
     */
    private final static Map<Integer, AlgorithmChat> privateMapOfActions;

    /**
     * index of the first message type of this algorithm.
     */
    private final int algorithmOffset = 0;

    /**
     * index of the action of this message type.
     */
    private final int actionIndex;

    /**
     * static block to build collections of actions.
     */
    static {
        privateMapOfActions = new HashMap<>();
        for (AlgorithmChat aa : AlgorithmChat.values()) {
            privateMapOfActions.put(aa.actionIndex, aa);
        }
        mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
    }

    /**
     * constructor of message type object.
     */
    private AlgorithmChat() {
        this.actionIndex = ListOfAlgorithmsClient.CHAT_CLIENT_START_INDEX + algorithmOffset + ordinal();
    }

    /**
     * obtains the index of this message type.
     */
    public int getActionIndex() {
        return actionIndex;
    }

    @Override
    public String toString() {
        return String.valueOf(actionIndex);
    }
}
