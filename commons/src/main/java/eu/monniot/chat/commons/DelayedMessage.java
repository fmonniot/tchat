package eu.monniot.chat.commons;

import java.io.Serializable;
import java.nio.channels.SelectionKey;

public class DelayedMessage {
    public final SelectionKey key;
    public final int messType;
    public final int identity;
    public final int seqNumber;
    public final Serializable msgSerializable;

    public DelayedMessage(SelectionKey key, int messType, int identity, int seqNumber, Serializable msgSerializable) {
        this.key = key;
        this.messType = messType;
        this.identity = identity;
        this.seqNumber = seqNumber;
        this.msgSerializable = msgSerializable;
    }

    @Override
    public String toString() {
        return "DelayedMessage{" +
                "msgSerializable=" + msgSerializable +
                ", identity=" + identity +
                ", messType=" + messType +
                '}';
    }
}
