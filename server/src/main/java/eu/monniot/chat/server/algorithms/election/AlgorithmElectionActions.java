package eu.monniot.chat.server.algorithms.election;

import java.io.IOException;

class AlgorithmElectionActions {

    /**
     * Implementation of the following algorithm: <pre>
if(caw == udef || message.initiator < caw)
    caw = message.initiator
    rec = 0
    father = message.sender
    forall(s : Neigh)
        if(s != father) send(TOKEN(identity, message.initiator), s)
if(message.initiator == caw)
    rec++
    if(rec == $Neigh)
        if(caw == identity)
            forall(q : Neigh)
                send(WINNER(identity, message.initiator), q)
        else
            send(TOKEN(identity, message.initiator), father)
     * </pre>
     * @param state state of the server
     * @param message message which had triggered this call
     * @return nothing
     */
    public static Object receiveToken(final ElectionServerState state, final TokenMessage message) {
        try {
            state.getSemaphore().acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(state.getCaw() < 0 || message.getInitiator() < state.getCaw()) {
            state.setCaw(message.getInitiator());
            state.setRec(0);
            state.setFather(message.getSender());
            try {
                state.getServer().sendToAllServersExceptOne(state.getFatherKey(),
                        AlgorithmElection.RECEIVE_TOKEN.getActionIndex(),
                        state.getIdentity(),
                        state.getSeqNumber(),
                        new TokenMessage(state.getIdentity(), message.getInitiator())
                        );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(message.getInitiator() == state.getCaw()) {
            state.setRec(state.getRec() + 1);
            if(state.getRec() == state.getNbNeighbours()) {
                try {
                    if(state.getCaw() == state.getIdentity()) {
                            state.getServer().sendToAllServers(
                                    AlgorithmElection.RECEIVE_WINNER.getActionIndex(),
                                    state.getIdentity(), state.getSeqNumber(),
                                    new WinnerMessage(state.getIdentity(), message.getInitiator()));
                    } else {
                        state.getServer().sendToAServer(state.getFatherKey(),
                                AlgorithmElection.RECEIVE_TOKEN.getActionIndex(),
                                state.getIdentity(), state.getSeqNumber(),
                                new TokenMessage(state.getIdentity(), message.getInitiator()));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        state.getSemaphore().release();
        return null;
    }
    /**
     * Implementation of the following algorithm: <pre>
if(lrec == 0 && message.initiator != identity)
     forall(q : Neigh)
         send(WINNER(identity, message.initiator), q)
lrec++
win = message.initiator
if(lrec == #Neigh)
    (win == identity) ? state = WINNER : state = LOSER</pre>
     *
     * @param state state of the server
     * @param message message which had triggered this call
     * @return nothing
     */
    public static Object receiveWinner(final ElectionServerState state, final WinnerMessage message) {
        final int identity = state.getIdentity();
        try {
            state.getSemaphore().acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(state.getLrec() == 0 && message.getInitiator() != identity) {
            try {
                state.getServer().sendToAllServers(AlgorithmElection.RECEIVE_WINNER.getActionIndex(),
                        identity, state.getSeqNumber(),
                        new WinnerMessage(identity, message.getInitiator()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        state.setLrec(state.getLrec()+1);
        state.setWin(message.getInitiator());

        if(state.getLrec() == state.getNbNeighbours()) {
            if(state.getWin() == identity) {
                state.setState(ElectionServerState.WINNER);
            } else {
                state.setState(ElectionServerState.LOSER);
            }
        }

        state.getSemaphore().release();
        // Return value unused
        return null;
    }
}
