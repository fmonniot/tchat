package eu.monniot.chat.client.algorithms.chat;


import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.commons.VectorClock;

/**
 * This class defines the content of a chat message.
 *
 * @author Denis Conan
 */
public class ChatMessage extends AbstractMessage {
    private static final long serialVersionUID = 1L;
    private final int sender;
    private final String content;
    private final VectorClock clock;

    public ChatMessage(final int sender, final String content, final VectorClock clock) {
        this.sender = sender;
        this.content = content;
        this.clock = clock;
    }

    public int getSender() {
        return sender;
    }

    public VectorClock getClock() {
        return clock;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "sender=" + sender +
                ", content='" + content + '\'' +
                ", clock=" + clock +
                '}';
    }
}
