package eu.monniot.chat.commons;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * This class defines a message as a set of byte buffers.
 * <p/>
 * One message is :
 * <ul>
 * <li>
 * the message type as an int</li>
 * <li>
 * the identity of the sender as an int</li>
 * <li>
 * the sequence number of sender as an int</li>
 * <li>
 * the message size as an int</li>
 * <li>
 * the data as a serialized object</li>
 * </ul>
 *
 * @author chris
 * @author Denis Conan
 */
public class FullDuplexMsgWorker {

    private static final Logger logger = LogManager.getLogger();

    /**
     * boolean allowing to add some debug printing.
     */
    protected boolean debug = false;

    /**
     * this arrays can contain message headers in the first buffer (fixed size)
     * and message body which size is described in the header. We need two
     * ByteBuffers due to asynchronism in input and output. Put and get
     * operations are not done at once.
     */
    private final ByteBuffer[] inBuffers;

    private final ByteBuffer[] outBuffers;

    /**
     * read message status, to describe completeness of data reception.
     */
    private ReadMessageStatus readState;
    private SocketChannel rwChan = null;
    private int inType, outType;
    private int inSize, outSize;
    private int inIdentity, outIdentity;
    private int inSeqNumber, outSeqNumber;

    /**
     * public constructor for an open channel---i.e., after accept.
     *
     * @param clientChan the socketChannel that has been accepted on server.
     */
    public FullDuplexMsgWorker(final SocketChannel clientChan) {
        inBuffers = new ByteBuffer[2];
        outBuffers = new ByteBuffer[2];
        inBuffers[0] = ByteBuffer.allocate(Integer.SIZE * 4 / Byte.SIZE);
        outBuffers[0] = ByteBuffer.allocate(Integer.SIZE * 4 / Byte.SIZE);
        inBuffers[1] = null;
        outBuffers[1] = null;
        readState = ReadMessageStatus.ReadUnstarted; // not yet started to read
        rwChan = clientChan;
    }

    /**
     * configure the channel in non blocking mode.
     */
    public void configureNonBlocking() throws IOException {
        rwChan.configureBlocking(false);
    }

    /**
     * get the current channel of this worker.
     *
     * @return my channel
     */
    public SocketChannel getChannel() {
        return rwChan;
    }

    /**
     * send a message using channel.
     *
     * @param type message type.
     * @param s    the message content is a Serializable.
     *
     * @return size of the data send.
     *
     * @throws IOException
     */
    public long sendMsg(final int type, final int identity,
                        final int seqNumber, final Serializable s) throws IOException {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        int size;
        ObjectOutputStream oo = new ObjectOutputStream(bo);
        oo.writeObject(s);
        oo.close();
        size = bo.size();
        setHeader(type, identity, seqNumber, size);
        outBuffers[1] = ByteBuffer.allocate(size);
        outBuffers[1].put(bo.toByteArray());
        bo.close();
        outBuffers[1].flip();
        sendBuffers();
        return size;
    }

    /**
     * send the buffers through the connection.
     */
    void sendBuffers() throws IOException {
        rwChan.write(outBuffers);
    }

    /**
     * initialize header with size and type.
     */
    void setHeader(final int type, final int identity,
                   final int seqNumber, final int size) {
        outType = type;
        outSize = size;
        outIdentity = identity;
        outSeqNumber = seqNumber;
        outBuffers[0].clear();
        outBuffers[0].putInt(outType);
        outBuffers[0].putInt(outIdentity);
        outBuffers[0].putInt(outSeqNumber);
        outBuffers[0].putInt(outSize);
        outBuffers[0].flip();
    }

    public void setDataByteBuffer(ByteBuffer outData) {
        outBuffers[1] = outData;
    }

    /**
     * close the channel.
     *
     * @throws IOException
     */
    public void close() throws IOException {
        rwChan.close();
    }

    /**
     * reads a message.
     *
     * @return a ReadMessageStatus to specify read progress.
     */
    public ReadMessageStatus readMessage() {
        int recvSize;
        if (readState == ReadMessageStatus.ReadUnstarted) {
            inBuffers[0].clear();
            readState = ReadMessageStatus.ReadHeaderStarted;
        }
        if (readState == ReadMessageStatus.ReadDataCompleted) {
            inBuffers[0].clear();
            inBuffers[1] = null;
            readState = ReadMessageStatus.ReadHeaderStarted;
        }
        if (readState == ReadMessageStatus.ReadHeaderStarted) {
            if (inBuffers[0].position() < inBuffers[0].capacity() - 1) {
                try {
                    recvSize = rwChan.read(inBuffers[0]);
                    logger.trace("	Received       : " + recvSize);
                    if (recvSize == 0) {
                        return readState;
                    }
                    if (recvSize < 0) {
                        readState = ReadMessageStatus.ChannelClosed;
                        close();
                        return readState;
                    }
                    if (inBuffers[0].position() < inBuffers[0].capacity() - 1) {
                        return readState;
                    }
                } catch (IOException ie) {
                    readState = ReadMessageStatus.ChannelClosed;
                    try {
                        close();
                    } catch (IOException ignored) {
                    }

                    return readState;
                }
            }
            inBuffers[0].flip();
            logger.trace("Position and limit : " + inBuffers[0].position() + " " + inBuffers[0].limit());
            inType = inBuffers[0].getInt();
            inIdentity = inBuffers[0].getInt();
            inSeqNumber = inBuffers[0].getInt();
            inSize = inBuffers[0].getInt();
            logger.trace("Message type and size : " + inType + " " + inSize);
            inBuffers[0].rewind();
            readState = ReadMessageStatus.ReadHeaderCompleted;
        }
        if (readState == ReadMessageStatus.ReadHeaderCompleted) {
            if (inBuffers[1] == null || inBuffers[1].capacity() != inSize) {
                inBuffers[1] = ByteBuffer.allocate(inSize);
            }
            readState = ReadMessageStatus.ReadDataStarted;
        }
        if (readState == ReadMessageStatus.ReadDataStarted) {
            if (inBuffers[1].position() < inBuffers[1].capacity() - 1) {
                try {
                    recvSize = rwChan.read(inBuffers[1]);
                    logger.trace("	Received       : " + recvSize);
                    if (recvSize == 0) {
                        return readState;
                    }
                    if (recvSize < 0) {
                        close();
                        readState = ReadMessageStatus.ChannelClosed;
                        return readState;
                    }
                } catch (IOException ie) {
                    readState = ReadMessageStatus.ChannelClosed;
                    try {
                        close();
                    } catch (IOException ignored) {
                    }

                    return readState;
                }
            }
            logger.trace("Position and capacity : " + inBuffers[1].position() + " " + inBuffers[1].capacity());
            if (inBuffers[1].position() >= inBuffers[1].capacity() - 1) {
                readState = ReadMessageStatus.ReadDataCompleted;
            }
        }
        return readState;
    }

    /**
     * return the Serializable data build out of the data part of the received
     * message when the readState is ReadDataCompleted. This operation should be
     * stateless for the ByteBuffers, meaning that we can getData and after
     * write the ByteBuffer if necessary.
     *
     * @return unserialized data.
     */
    public Serializable getData() throws IOException {
        Serializable res = null;
        if (readState == ReadMessageStatus.ReadDataCompleted) {
            try {
                inBuffers[1].flip();
                ByteArrayInputStream bi = new ByteArrayInputStream(
                        inBuffers[1].array());
                ObjectInputStream oi = new ObjectInputStream(bi);
                res = (Serializable) oi.readObject();
                oi.close();
                bi.close();
            } catch (ClassNotFoundException ce) {
                ce.printStackTrace();
            }
        }
        inBuffers[1].rewind();
        return res;
    }

    /**
     * get direct access to ByteBuffers.
     *
     * @return the ByteBuffers.
     */
    public ByteBuffer[] getInBuffers() {
        return inBuffers;
    }

    public int getInType() {
        return inType;
    }

    public int getOutType() {
        return outType;
    }

    public int getInSize() {
        return inSize;
    }

    public int getOutSize() {
        return outSize;
    }

    public int getInIdentity() {
        return inIdentity;
    }

    public int getOutIdentity() {
        return outIdentity;
    }

    public int getInSeqNumber() {
        return inSeqNumber;
    }

    public int getOutSeqNumber() {
        return outSeqNumber;
    }

    /**
     * passThroughOutBuffers is used to send prepared ByteBuffers through a
     * channel.
     *
     * @param setBbs ByteBuffers to send.
     *
     * @throws IOException
     */
    public void passOutBuffers(ByteBuffer[] setBbs) throws IOException {
        outBuffers[0].clear();
        setBbs[0].rewind();
        do {
            outBuffers[0].put(setBbs[0].get());
        } while (setBbs[0].position() < setBbs[0].capacity());
        outBuffers[1] = setBbs[1];
        outBuffers[0].flip();
        outBuffers[1].rewind();
        rwChan.write(outBuffers);
    }
}
