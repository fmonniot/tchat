package eu.monniot.chat.commons;

/**
 * This Enumeration type declares the status of the reception of a message.
 *
 * @author chris
 * @author Denis Conan
 */
public enum ReadMessageStatus {
    ReadUnstarted,
    ReadHeaderStarted,
    ReadHeaderCompleted,
    ReadDataStarted,
    ReadDataCompleted,
    ChannelClosed
}
