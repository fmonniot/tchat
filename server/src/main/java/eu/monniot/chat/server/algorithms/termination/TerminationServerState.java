package eu.monniot.chat.server.algorithms.termination;

import eu.monniot.chat.commons.State;
import eu.monniot.chat.server.ChatServerMain;

import java.nio.channels.SelectionKey;

public interface TerminationServerState extends State {

    /**
     * Server is in state passive
     */
    int PASSIVE = 10;

    /**
     * Server is in state active
     */
    int ACTIVE = 11;

    /**
     * Token or server has color white
     */
    int WHITE = 1;

    /**
     * Token or server has color black
     */
    int BLACK = 2;

    /**
     * Increment the local message counter
     */
    void incrementMc();

    /**
     * Decrement the local message counter
     */
    void decrementMc();

    /**
     * @return the local message counter
     */
    int getMc();

    /**
     * Either {@link #ACTIVE} or {@link #PASSIVE}
     *
     * @param state the state of this node (termination detection algorithm)
     */
    void setTerminationState(int state);

    /**
     * Either {@link #ACTIVE} or {@link #PASSIVE}
     *
     * @return the state of this node (termination detection algorithm)
     */
    int getTerminationState();

    /**
     * Either {@link #WHITE} or {@link #BLACK}
     *
     * @param color the color of this token
     */
    void setColor(int color);

    /**
     * Either {@link #WHITE} or {@link #BLACK}
     *
     * @return the color of this token
     */
    int getColor();

    SelectionKey getNextServer();

    /**
     * @return the identity of p0
     */
    int getWin();

    /**
     * @return the identity of this node
     */
    int getIdentity();

    ChatServerMain getServer();

    /**
     * @return the maximum of counters of clientSeqNumbers set
     */
    int getSeqNumber();

    /**
     * Wait to be in a passive state to consume the given message.
     *
     * @param msg The message received
     */
    void waitPassiveState(TokenMessage msg);
}
