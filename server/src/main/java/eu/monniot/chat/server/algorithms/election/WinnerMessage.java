package eu.monniot.chat.server.algorithms.election;

/**
 * Message of type winner in the echo algorithm
 *
 * This message indicate the winner with its {@link #getInitiator()} method.
 */
public class WinnerMessage extends ElectionMessage {
    private static final long serialVersionUID = 1L;

    public WinnerMessage(int sender, int initiator) {
        super(sender, initiator);
    }

    @Override
    public String toString() {
        return "WinnerMessage{" +
                "sender=" + getSender() +
                ", initiator=" + getInitiator() +
                '}';
    }
}
