package eu.monniot.chat.commons;

public class ListOfAlgorithmsDelegate {

    /**
     * Search for the action to execute in the collection of actions of the algorithm of the client.
     * <br/>
     * When upgrading to java8 replace this with a default method in the interface
     * {@link eu.monniot.chat.commons.AlgorithmActionInterface}
     *
     * @param state       state of the client.
     * @param actionIndex index of the action to execute.
     * @param content     content of the message just received.
     *
     * @throws AlgorithmActionInvocationException exception thrown when either the content is not usable or the
     *                                            index correspond to no action.
     */
    public static void execute(State state, int actionIndex, Object content, ListOfAlgorithms[] algorithms)
            throws AlgorithmActionInvocationException {

        boolean executed = false;
        for (ListOfAlgorithms am : algorithms) {
            for (AlgorithmActionInterface action : am.getActions().values()) {
                if (action.getActionIndex() == actionIndex) {
                    executed = true;
                    AbstractMessage c;
                    if (content instanceof AbstractMessage) {
                        c = (AbstractMessage) content;
                    } else {
                        throw new AlgorithmActionInvocationException(
                                "The content is not of type AbstractContent: "
                                        + content.getClass().getName());
                    }
                    action.execute(state, c);
                }
            }
        }
        if (!executed) {
            throw new AlgorithmActionInvocationException("Unknown action: " + actionIndex);
        }
    }
}
