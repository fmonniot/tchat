package eu.monniot.chat.client;

import eu.monniot.chat.client.algorithms.chat.ChatMessage;
import eu.monniot.chat.commons.AbstractState;
import eu.monniot.chat.commons.VectorClock;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class defines the attributes of the state of the client in the state
 * machine. All the attributes are public and special care must be taken to
 * access these attributes when multi-threading: The semaphore must be used in
 * this case.
 *
 * @author Denis Conan
 */
public class ChatClientState extends AbstractState {

    /**
     * identity of this client.
     */
    public int identity;

    /**
     * number of chat messages received.
     */
    public int nbChatMessageContentReceived;

    /**
     * number of chat messages sent.
     */
    public int nbChatMessageContentSent;

    /**
     * The vector clock of this client
     */
    public VectorClock clock = new VectorClock();

    /**
     * The message bag used in the broadcast algorithm
     */
    public Collection<ChatMessage> msgBag = new ArrayList<>();

    /**
     * Control the main loop of {@link eu.monniot.chat.client.ChatClientMain#main(String[])}.
     * When it is false, the loop is alive.
     */
    public boolean exit = false;
}
