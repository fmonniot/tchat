package eu.monniot.chat.server.algorithms.mutualExclusion;

import eu.monniot.chat.commons.State;
import eu.monniot.chat.commons.VectorClock;
import eu.monniot.chat.server.ChatServerMain;

import java.nio.channels.SelectionKey;

public interface ExclusionServerState extends State {

    /**
     * @return identity of this server.
     */
    int getIdentity();

    /**
     * @return the token of this algorithm (or null if this node doesn't have it)
     */
    VectorClock getExclusionToken();

    /**
     * @param token the token of this algorithm
     */
    void setExclusionToken(VectorClock token);

    /**
     * @return logical clock of this node
     */
    int getNs();

    /**
     * increment the logical clock of this node
     */
    void incrementNs();

    /**
     * @return the table of token request received by this node
     */
    VectorClock getDem();

    /**
     * Store the max of the local clock with the given {@code identity} and {@code ns} (logical clock).
     *
     * @param identity the identity of the process to make the comparison
     * @param ns the logical clock to evaluate against
     */
    void maxDem(int identity, int ns);

    /**
     * @return is this node is in a critical section
     */
    boolean isCritical();

    /**
     * @param critical the critical node state of this node
     */
    void setCritical(boolean critical);

    /**
     * @return return the server (to be able to send message)
     */
    ChatServerMain getServer();

    /**
     * @return the sequence number to be able to send message
     */
    int getSeqNumber();

    /**
     * @param identity the identity of the SelectionKey wanted
     * @return the SelectionKey associated to the given identity
     */
    SelectionKey getSelectionKeyOf(int identity);

    /**
     * Set an action to be executed at the next reception of a
     * {@link eu.monniot.chat.server.algorithms.mutualExclusion.TokenMessage}.
     *
     * @param callback the action that will be executed
     */
    void setCriticalAction(Runnable callback);

    /**
     * This action remove the action after consumption.
     *
     * @return a critical action to execute.
     */
    Runnable consumeCriticalAction();
}
