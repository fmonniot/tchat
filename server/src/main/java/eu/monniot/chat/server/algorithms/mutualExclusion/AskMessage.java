package eu.monniot.chat.server.algorithms.mutualExclusion;

import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.server.algorithms.IdentityMessage;

/**
 * A message that request a token for the mutual exclusion algorithm (Ricart and Agalawa, 1983)
 */
public class AskMessage extends AbstractMessage implements IdentityMessage {
    private final int sender;
    private int ns;

    public AskMessage(final int sender, final int ns) {
        this.sender = sender;
        this.ns = ns;
    }

    /**
     * @return the logical clock of the sender
     */
    public int getNs() {
        return ns;
    }

    @Override
    public int getSender() {
        return sender;
    }

    @Override
    public String toString() {
        return "AskMessage{" +
                "sender=" + sender +
                ", ns=" + ns +
                '}';
    }
}
