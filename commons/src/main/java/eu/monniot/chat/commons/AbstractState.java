package eu.monniot.chat.commons;

import java.util.concurrent.Semaphore;

/**
 * This abstract class defines the type for a state of a state machine (client
 * or server).
 *
 * @author Denis Conan
 */
public abstract class AbstractState implements State {

    private final Semaphore semaphore = new Semaphore(1);

    @Override
    public Semaphore getSemaphore() {
        return semaphore;
    }
}
