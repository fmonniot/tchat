package eu.monniot.chat.server.algorithms.election;

import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.server.algorithms.IdentityMessage;

/**
 * Base class for message used in the election algorithm
 *
 * @see eu.monniot.chat.server.algorithms.election.TokenMessage
 * @see eu.monniot.chat.server.algorithms.election.WinnerMessage
 */
public abstract class ElectionMessage extends AbstractMessage implements IdentityMessage {

    private int mSender;
    private int mInitiator;

    public ElectionMessage(final int sender, final int initiator) {
        mSender = sender;
        mInitiator = initiator;
    }

    @Override
    public int getSender() {
        return mSender;
    }

    /**
     * @return the initiator's identity of the wave of this message
     */
    public int getInitiator() {
        return mInitiator;
    }
}
