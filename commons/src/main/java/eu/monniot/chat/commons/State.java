package eu.monniot.chat.commons;

import java.util.concurrent.Semaphore;

public interface State {
    /**
     * semaphore to protect concurrent access of server's state
     * <p/>
     * NB: concurrency exist when a thread (e.g., a ChatSelectorMultiServer) is
     * created on the server process to allow for external control of the server
     * through command lines on the console.
     */
    Semaphore getSemaphore();
}
