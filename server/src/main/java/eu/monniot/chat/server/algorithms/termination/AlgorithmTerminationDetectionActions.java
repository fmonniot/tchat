package eu.monniot.chat.server.algorithms.termination;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.channels.SelectionKey;

public class AlgorithmTerminationDetectionActions {

    /**
     * Delay to wait before sending the token to the next server
     */
    public static final int SEND_DELAY = 1000;

    private static Logger logger = LogManager.getLogger();

    /**
     * Action to execute when the algorithm detect a termination
     */
    private static Runnable announceAction = null;

    /**
     * Send the token to the next server after {@code SEND_DELAY}ms.
     *
     * @param state the state of the server
     * @param token the token receive
     */
    private static void sendToken(final TerminationServerState state, final TokenMessage token) {
        Thread senderThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(SEND_DELAY);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SelectionKey key = state.getNextServer();
                logger.debug("sendToken " + token + " to " + key);

                try {
                    state.getServer().sendToAServer(key,
                            AlgorithmTerminationDetection.RECEIVE_TOKEN.getActionIndex(),
                            state.getIdentity(), state.getSeqNumber(), token
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        senderThread.setName("senderTermination");
        senderThread.start();
    }

    /**
     * Called when a termination is detected.
     */
    private static void announce() {
        logger.info("Termination detected.");
        if(announceAction != null) {
            logger.info("Run termination action.");
            announceAction.run();
        }
    }

    /**
     * Action executed when a {@link eu.monniot.chat.server.algorithms.termination.TokenMessage} is received.
     *
     * @param state the state of the server
     * @param token the token received
     * @return null
     */
    public static Object receiveToken(TerminationServerState state, TokenMessage token) {
        if(state.getTerminationState() != TerminationServerState.PASSIVE) {
            logger.debug("wait for next passive state");
            state.waitPassiveState(token);
            return null;
        }

        logger.trace("receiveToken <- p=" + state.getIdentity() +
                ", p0=" + state.getWin() +
                ", c=" + token.getColor() +
                ", cp=" + state.getColor() +
                ", mc="+state.getMc() +
                ", q="+token.getQ());
        if(state.getIdentity() == state.getWin()) {
            if(token.getColor() == TerminationServerState.WHITE &&
                    state.getColor() == TerminationServerState.WHITE &&
                    (state.getMc() + token.getQ() == 0)){
                announce();
            } else {
                sendToken(state, new TokenMessage(TerminationServerState.WHITE, 0));
            }
        } else {
            if(token.getColor() == TerminationServerState.WHITE) {
                sendToken(state, new TokenMessage(token.getColor(), token.getQ() + state.getMc()));
            } else {
                sendToken(state, new TokenMessage(TerminationServerState.BLACK, token.getQ() + state.getMc()));
            }
        }
        state.setColor(TerminationServerState.WHITE);

        logger.trace("receiveToken -> p=" + state.getIdentity() +
                ", p0=" + state.getWin() +
                ", c=" + token.getColor() +
                ", cp=" + state.getColor() +
                ", mc="+state.getMc() +
                ", q="+token.getQ());
        return null;
    }

    /**
     * Must be called whenever a user message is sent
     *
     * @param state the state of the server
     */
    public static void send(TerminationServerState state) {
        logger.debug("send");
        state.incrementMc();
    }

    /**
     * Must be called before a user message is received
     *
     * @param state the state of the server
     */
    public static void beforeReceive(TerminationServerState state) {
        logger.debug("beforeReceive");
        state.setTerminationState(TerminationServerState.PASSIVE);
    }

    /**
     * Must be called whenever a user message is received
     *
     * @param state the state of the server
     */
    public static void receive(TerminationServerState state) {
        logger.debug("receive <- p=" + state.getIdentity() +
                ", p0=" + state.getWin() +
                ", cp=" + state.getColor() +
                ", mc="+state.getMc());
        state.setTerminationState(TerminationServerState.ACTIVE);
        state.decrementMc();
        state.setColor(TerminationServerState.BLACK);
    }

    /**
     * Launch the algorithm
     *
     * @param state the state of the server
     */
    public static void init(TerminationServerState state) {
        logger.debug("init");
        sendToken(state, new TokenMessage(TerminationServerState.WHITE, 0));
    }

    /**
     *
     * @param state the state of the server
     * @param callback the callback to be executed when the {@link #announce()} method is called
     */
    public static void init(TerminationServerState state, Runnable callback) {
        announceAction = callback;
        init(state);
    }
}
