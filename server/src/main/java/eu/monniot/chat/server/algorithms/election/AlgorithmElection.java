package eu.monniot.chat.server.algorithms.election;

import eu.monniot.chat.commons.*;
import eu.monniot.chat.server.algorithms.ListOfAlgorithmsServer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This Enumeration type declares the algorithm of the election part of the
 * server's state machine. Only one message content type can be received.
 *
 * @author Denis Conan
 */
public enum AlgorithmElection implements AlgorithmActionInterface {

    RECEIVE_TOKEN(){
        @Override
        public Object execute(State state, AbstractMessage msg) throws AlgorithmActionInvocationException {
            if (!(msg instanceof TokenMessage))
                throw new AlgorithmActionInvocationException("message must be a TokenMessage.");
            if(!(state instanceof ElectionServerState))
                throw new IllegalStateException("state must implements ElectionServerState.");

            return AlgorithmElectionActions.receiveToken((ElectionServerState) state, (TokenMessage) msg);
        }
    },
    RECEIVE_WINNER(){
        @Override
        public Object execute(State state, AbstractMessage msg) throws AlgorithmActionInvocationException {
            if (!(msg instanceof WinnerMessage))
                throw new AlgorithmActionInvocationException("message must be a WinnerMessage.");

            if(!(state instanceof ElectionServerState))
                throw new IllegalStateException("state must implements ElectionServerState.");

            return AlgorithmElectionActions.receiveWinner((ElectionServerState) state, (WinnerMessage) msg);
        }
    };

    /**
     * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
     */
    public final static Map<Integer, AlgorithmElection> mapOfActions;

    /**
     * collection of the actions of this algorithm. The Key is the index of the
     * corresponding message type. This collection is kept private to avoid
     * modifications.
     */
    private final static Map<Integer, AlgorithmElection> privateMapOfActions;

    /**
     * index of the first message type of this algorithm.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final int algorithmOffset = 0;

    /**
     * index of the action of this message type.
     */
    private final int actionIndex;

    /**
     * static block to build collections of actions.
     */
    static {
        privateMapOfActions = new HashMap<>();
        for (AlgorithmElection algorithmElection : AlgorithmElection.values()) {
            privateMapOfActions.put(algorithmElection.actionIndex, algorithmElection);
        }
        mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
    }

    /**
     * constructor of message type object.
     */
    private AlgorithmElection() {
        this.actionIndex = ListOfAlgorithmsServer.CHAT_SERVER_START_INDEX + algorithmOffset + ordinal();
    }

    /**
     * obtains the index of this message type.
     */
    @Override
    public int getActionIndex() {
        return actionIndex;
    }

    @Override
    public String toString() {
        return String.valueOf(actionIndex);
    }
}
