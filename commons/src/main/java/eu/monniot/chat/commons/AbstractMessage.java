package eu.monniot.chat.commons;

import java.io.Serializable;

/**
 * This abstract class is the root of the message contents used in the state
 * machines (client or server).
 *
 * @author Denis Conan
 */
public abstract class AbstractMessage implements Serializable {
    private static final long serialVersionUID = 1L;
}
