package eu.monniot.chat.server;

import eu.monniot.chat.commons.AbstractState;
import eu.monniot.chat.commons.VectorClock;
import eu.monniot.chat.server.algorithms.election.ElectionServerState;
import eu.monniot.chat.server.algorithms.mutualExclusion.ExclusionServerState;
import eu.monniot.chat.server.algorithms.termination.AlgorithmTerminationDetectionActions;
import eu.monniot.chat.server.algorithms.termination.TerminationServerState;
import eu.monniot.chat.server.algorithms.termination.TokenMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


class ChatServerState extends AbstractState
        implements ElectionServerState, ExclusionServerState, TerminationServerState {

    private static Logger logger = LogManager.getLogger();

    /**
     * backward reference to the server selector object in order to use its
     * methods to send messages.
     */
    public ChatServerMain server;

    /**
     * @see #getSelectionKeyOf(int)
     */
    public Map<Integer, SelectionKey> identitiesMap = new TreeMap<>();

    @Override
    public ChatServerMain getServer() {
        return server;
    }

    /**
     * selection key of the connection from which the last message was received.
     */
    public SelectionKey currKey;

    /**
     * identity of this server.
     */
    public int identity;

    @Override
    public int getIdentity() {
        return identity;
    }

    /**
     * one counter per client in order to control the propagation of client
     * messages: stop forward to remote servers when the message has already
     * been forwarded ; the counter is set by the server receiving the message
     * from the client
     */
    public HashMap<Integer, Integer> clientSeqNumbers;

    /**
     * seqNumber is equal to the maximum of counters of clientSeqNumbers set
     */
    public int seqNumber;

    @Override
    public int getSeqNumber() {
        return seqNumber;
    }

    /**
     * the number of neighbours in the cluster
     */
    public int nbNeighbours = 0;

    @Override
    public int getNbNeighbours() {
        return nbNeighbours;
    }

    ////////////////////////////////////////////////
    //               Election state               //
    ////////////////////////////////////////////////

    public SelectionKey fatherKey;

    private int caw = -1;
    private int win = -1;
    private int rec = 0;
    private int lrec = 0;

    @Override
    public int getCaw() {
        return caw;
    }

    @Override
    public void setCaw(int caw) {
        this.caw = caw;
    }

    @Override
    public SelectionKey getFatherKey() {
        return fatherKey;
    }

    @Override
    public void setFather(int father) {
    }

    @Override
    public int getWin() {
        return win;
    }

    @Override
    public void setWin(int win) {
        this.win = win;
    }

    @Override
    public int getRec() {
        return rec;
    }

    @Override
    public void setRec(int rec) {
        this.rec = rec;
    }

    @Override
    public int getLrec() {
        return lrec;
    }

    @Override
    public void setLrec(int lrec) {
        this.lrec = lrec;
    }

    @Override
    public void setState(int state) {
        logger.info("Election "+ ((state == WINNER) ? "Winner" : "Loser") +".");
        if(state == WINNER) {
            logger.debug(" Create a token for mutual exclusion (of size "+identitiesMap.size()+")");
            exclusionToken = new VectorClock();

            logger.debug(" Init termination detection from this node (" + getIdentity() + ")");
            AlgorithmTerminationDetectionActions.init(this);
        }

        for (int i = 0; i <= identitiesMap.size(); i++) {
            if(exclusionToken != null) exclusionToken.getEntry(i);
            dem.getEntry(i);
        }

        if(state == WINNER) logger.debug("Created exclusion token: "+exclusionToken);
        logger.debug("Populated exclusion dem table: "+dem);
    }

    /////////////////////////////////////////////////
    //               Exclusion state               //
    /////////////////////////////////////////////////

    private VectorClock exclusionToken = null;
    private int ns = 0;
    private VectorClock dem = new VectorClock();
    private boolean critical = false;
    private Runnable criticalAction;

    @Override
    public VectorClock getExclusionToken() {
        return exclusionToken;
    }

    @Override
    public void setExclusionToken(VectorClock token) {
        exclusionToken = token;
    }

    @Override
    public int getNs() {
        return ns;
    }

    @Override
    public void incrementNs() {
        ns++;
    }

    @Override
    public VectorClock getDem() {
        return dem;
    }

    @Override
    public void maxDem(int identity, int ns) {
        if(ns > dem.getEntry(identity)) {
            dem.setEntry(identity, ns);
        }
    }

    @Override
    public boolean isCritical() {
        return critical;
    }

    @Override
    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    @Override
    public SelectionKey getSelectionKeyOf(int identity) {
        return identitiesMap.get(identity);
    }

    @Override
    public void setCriticalAction(Runnable callback) {
        criticalAction = callback;
    }

    @Override
    public Runnable consumeCriticalAction() {
        Runnable ret = criticalAction;
        criticalAction = null;
        return ret;
    }

    /////////////////////////////////////////////////
    //              Termination state              //
    /////////////////////////////////////////////////

    private int mc = 0;
    private int terminationState;
    private int color = TerminationServerState.WHITE;
    private TokenMessage waitingToken;

    @Override
    public void incrementMc() {
        mc++;
    }

    @Override
    public void decrementMc() {
        mc--;
    }

    @Override
    public int getMc() {
        return mc;
    }

    @Override
    public void setTerminationState(int state) {
        logger.trace("Change termination state to " + (state==TerminationServerState.ACTIVE ? "active" : "passive" ));
        if(state == TerminationServerState.ACTIVE) {
            this.terminationState = state;
        } else if(state == TerminationServerState.PASSIVE) {
            this.terminationState = state;
            if(waitingToken != null) {
                AlgorithmTerminationDetectionActions.receiveToken(this, waitingToken);
                waitingToken = null;
            }
        }
    }

    @Override
    public int getTerminationState() {
        return terminationState;
    }

    @Override
    public void setColor(int color) {
        if(color == TerminationServerState.WHITE || color == TerminationServerState.BLACK) {
            this.color = color;
        }
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public SelectionKey getNextServer() {
        return identitiesMap.get((getIdentity()+1) % (identitiesMap.size() + 1));
    }

    @Override
    public void waitPassiveState(TokenMessage token) {
        waitingToken = token;
    }
}
