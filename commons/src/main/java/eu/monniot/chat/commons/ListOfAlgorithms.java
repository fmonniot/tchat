package eu.monniot.chat.commons;

import java.util.Map;

public interface ListOfAlgorithms {

    /**
     * @return collection of the actions of this algorithm.
     */
    Map<Integer, ? extends AlgorithmActionInterface> getActions();
}
