package eu.monniot.chat.server.algorithms.mutualExclusion;

import eu.monniot.chat.commons.VectorClock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * List of actions executed by the Ricart and Agalawa, 1983 algorithm
 */
public class AlgorithmExclusionActions {
    private static Logger logger = LogManager.getLogger();

    /**
     * Method executed when a request token message is received
     *
     * @param state the state of the server
     * @param message the message received
     * @return null
     */
    public static Object receiveAsk(final ExclusionServerState state, final AskMessage message) {
        state.maxDem(message.getSender(), message.getNs());

        logger.debug("receiveAsk with message " + message);
        logger.debug("Current state: isCritical=" + state.isCritical() + ", token=" + state.getExclusionToken());

        if(state.getExclusionToken() != null && !state.isCritical()) {
            leaveCriticalSection(state);
        }

        return null;
    }

    /**
     * Method executed when the token message is received
     *
     * @param state the state of the server
     * @param message the message received
     * @return null
     */
    public static Object receiveToken(final ExclusionServerState state, final TokenMessage message) {

        state.setExclusionToken(message.getClock());

        Runnable criticalAction = state.consumeCriticalAction();
        if (criticalAction != null) {
            criticalAction.run();
            leaveCriticalSection(state);
        }

        return null;
    }

    /**
     * Ask to enter the critical section to execute the given {@code callback}
     *
     * @param state the state of the server
     * @param callback the callback which will be called once in the critical section
     */
    public static void enterCriticalSection(final ExclusionServerState state, final Runnable callback) {
        if(state.getExclusionToken() == null) {
            state.incrementNs();
            try {
                state.setCriticalAction(callback);
                state.getServer().sendToAllServers(
                        AlgorithmExclusion.RECEIVE_ASK.getActionIndex(),
                        state.getIdentity(),
                        state.getSeqNumber(),
                        new AskMessage(state.getIdentity(), state.getNs()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            state.setCritical(true);
            callback.run();
            leaveCriticalSection(state);
        }
    }

    /**
     * Called after a critical action have finnish
     *
     * @param state the state of the server
     */
    protected static void leaveCriticalSection(final ExclusionServerState state) {
        final int identity = state.getIdentity();
        final VectorClock dem = state.getDem();
        VectorClock jet = state.getExclusionToken();

        jet.setEntry(identity, state.getNs());
        state.setCritical(false);

        logger.debug("leaveCriticalSection: dem=" + dem);
        logger.trace("begin loop: i=" + (identity + 1) +
                "; i == " + identity +
                "; i = (i+1)%" + dem.size());

        for(int i = ((identity +1)% dem.size()); i != identity; i = (i+1)% dem.size()) {
            logger.trace("  in loop: i=" + i + " , i == " + identity);
            logger.trace("  if(" + dem.getEntry(i) + " > " + jet.getEntry(i) +
                    " && " + state.getExclusionToken() + " != null)");

            if(dem.getEntry(i) > jet.getEntry(i) && state.getExclusionToken() != null) {
                try {
                    logger.debug("  send token to server " + i);
                    state.getServer().sendToAServer(state.getSelectionKeyOf(i),
                            AlgorithmExclusion.RECEIVE_TOKEN.getActionIndex(),
                            identity, state.getSeqNumber(), new TokenMessage(jet)
                    );
                    state.setExclusionToken(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
