package eu.monniot.chat.commons;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;

/**
 * Class defining a vector clock with a Map.
 */
public final class VectorClock implements Serializable, Cloneable {

    private static final Logger logger = LogManager.getLogger();

    private static final long serialVersionUID = 1L;

    private Map<Integer, Integer> vectorClock;

    public VectorClock() {
        vectorClock = new Hashtable<>();
    }

    public Integer getEntry(final Integer key) {
        Integer entry = vectorClock.get(key);
        if (entry == null) {
            entry = 0;
            vectorClock.put(key, entry);
            logger.trace("Vector clock: getEntry, unknown entry '" + key + "', added");
        }
        return entry;
    }

    public void setEntry(final Integer key, final Integer value) {
        Integer entry = vectorClock.get(key);
        if (entry == null) {
            vectorClock.put(key, value);
            logger.trace("Vector clock: setEntry, unknown entry '" + key + "', added with value " + value);
        } else {
            vectorClock.put(key, value);
        }
    }

    public void incrementEntry(final Integer key) {
        Integer entry = vectorClock.get(key);
        if (entry == null) {
            vectorClock.put(key, 1);
            logger.trace("Vector clock: incrementEntry, unknown entry '" + key + "', added and value = 0");
        } else {
            int oldValue = entry;
            vectorClock.remove(key);
            vectorClock.put(key, oldValue + 1);
        }
    }

    @Override
    public Object clone() {
        VectorClock clone = null;
        try {
            clone = (VectorClock) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if (clone != null) {
            clone.vectorClock = new Hashtable<>();
            for (Integer key : vectorClock.keySet()) {
                Integer entry = vectorClock.get(key);
                clone.vectorClock.put(key, entry);
            }
        }
        return clone;
    }

    public void max(final VectorClock other) {
        if (other != null) {
            Map<Integer, Integer> temp = ((VectorClock) this.clone()).vectorClock;
            Map<Integer, Integer> otherTemp = ((VectorClock) other.clone()).vectorClock;
            for (Integer key : temp.keySet()) {
                Integer entry = temp.get(key);
                Integer otherEntry = otherTemp.get(key);
                if ((otherEntry != null) && (otherEntry > entry)) {
                    setEntry(key, otherEntry);
                }
            }
            for (Integer key : otherTemp.keySet()) {
                Integer entry = temp.get(key);
                Integer otherEntry = otherTemp.get(key);
                if ((entry == null) || (otherEntry > entry)) {
                    setEntry(key, otherEntry);
                }
            }
        }
    }

    public boolean isPrecededByAndFIFO(final VectorClock msgVC, final int sender) {
        if (msgVC.getEntry(sender) != (this.getEntry(sender) + 1)) {
            return false;
        }
        for (Integer key : vectorClock.keySet()) {
            if (key != sender) {
                if (msgVC.getEntry(key) > this.getEntry(key)) {
                    return false;
                }
            }
        }
        return true;
    }

    public String toString() {
        return vectorClock.toString();
    }

    /**
     * Returns the number of key-value mappings in this map.
     * If the map contains more than Integer.MAX_VALUE elements, returns Integer.MAX_VALUE.
     * @return the number of key-value mappings in this map
     */
    public int size() {
        return vectorClock.size();
    }
}
