package eu.monniot.chat.server.algorithms.election;

import eu.monniot.chat.commons.State;
import eu.monniot.chat.server.ChatServerMain;

import java.nio.channels.SelectionKey;

public interface ElectionServerState extends State {

    /**
     * State of this server during the election algorithm.
     * Sleeping: algorithm isn't terminated yet.
     */
    int SLEEPING = 10;

    /**
     * State of this server during the election algorithm.
     * Winner: algorithm is terminated and we're the master of this cluster.
     */
    int WINNER = 11;

    /**
     * State of this server during the election algorithm.
     * Loser: algorithm is terminated and we're a slave in this cluster.
     */
    int LOSER = 12;

    /**
     * reference to the server selector object in order to use its
     * methods to send messages.
     *
     * @return the server
     */
    ChatServerMain getServer();

    /**
     * @return identity of this server.
     */
    int getIdentity();

    /**
     * @return the Currently Active Wave
     */
    int getCaw();

    /**
     * @param caw the Currently Active Wave
     */
    void setCaw(int caw);

    /**
     * @return the {@link java.nio.channels.SelectionKey} associated with the father node
     */
    SelectionKey getFatherKey();

    /**
     * @param father father node in the caw
     */
    void setFather(int father);

    /**
     * @return identity of the winner
     */
    int getWin();

    /**
     * @param win identity of the winner
     */
    void setWin(int win);

    /**
     * @return Number of {@link TokenMessage} received
     */
    int getRec();

    /**
     * @param rec Number of {@link TokenMessage} received
     */
    void setRec(int rec);

    /**
     * @return Number of {@link WinnerMessage} received
     */
    int getLrec();

    /**
     * @param lrec Number of {@link WinnerMessage} received
     */
    void setLrec(int lrec);

    /**
     * @param state State of this node. Either {@link #SLEEPING}, {@link #WINNER} or {@link #LOSER}
     */
    void setState(int state);

    /**
     * @return the maximum of counters of clientSeqNumbers set
     */
    int getSeqNumber();

    /**
     * @return the number of neighbours in the cluster
     */
    int getNbNeighbours();
}
