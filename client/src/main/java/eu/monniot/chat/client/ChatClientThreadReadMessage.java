package eu.monniot.chat.client;

import eu.monniot.chat.client.algorithms.ListOfAlgorithmsClient;
import eu.monniot.chat.commons.AlgorithmActionInvocationException;
import eu.monniot.chat.commons.FullDuplexMsgWorker;
import eu.monniot.chat.commons.ReadMessageStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.channels.SocketChannel;

/**
 * This class contains the chat client's thread waiting for messages from its
 * server.
 *
 * @author chris
 * @author Denis Conan
 */
class ChatClientThreadReadMessage extends FullDuplexMsgWorker implements Runnable {

    private static final Logger logger = LogManager.getLogger();

    /**
     * state of the client's state machine
     */
    private final ChatClientState clientState;
    private boolean run = true;

    public ChatClientThreadReadMessage(final SocketChannel chan, ChatClientState state, boolean debug) throws IOException {
        super(chan);
        this.debug = debug;
        clientState = state;
        ReadMessageStatus msgState;
        do {
            msgState = readMessage();
        } while (msgState != ReadMessageStatus.ReadDataCompleted);
        clientState.identity = (Integer) getData();
    }

    public void run() {
        ReadMessageStatus messState;
        while (run) {
            Object content = null;
            // read and println a message
            try {
                messState = readMessage();
                if (messState == ReadMessageStatus.ChannelClosed) {
                    break;
                } else {
                    if (messState == ReadMessageStatus.ReadDataCompleted) {
                        content = getData();
                        ListOfAlgorithmsClient.execute(clientState, getInType(), content);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            } catch (AlgorithmActionInvocationException e) {
                e.printStackTrace();
            }
            logger.info("End of reception of a message ("+ (content != null ? content.getClass().getSimpleName() : "") +")");
        }
    }

    @Override
    public void close() throws IOException {
        super.close();
        this.run = false;
    }
}
