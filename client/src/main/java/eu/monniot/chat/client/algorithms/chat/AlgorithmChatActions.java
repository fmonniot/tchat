package eu.monniot.chat.client.algorithms.chat;

import eu.monniot.chat.client.ChatClientState;
import eu.monniot.chat.commons.AlgorithmActionInvocationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class defines the methods implementing the reaction of the state machine
 * part concerning the reception of chat messages. Since only one message
 * content type is declared in the algorithm, there is only one state method in
 * this class.
 *
 * @author Denis Conan
 */
class AlgorithmChatActions {
    private static final Logger logger = LogManager.getLogger();

    /**
     * Executed each time the client received a {@link eu.monniot.chat.client.algorithms.chat.ChatMessage}
     *
     * @param state the state of the client
     * @param message the message received
     * @return null
     *
     * @throws AlgorithmActionInvocationException
     */
    public static Object receiveChatMessage(final ChatClientState state, final ChatMessage message)
            throws AlgorithmActionInvocationException {

        if (message == null) throw new IllegalArgumentException("Try executing action with null message");

        if (state == null) throw new IllegalArgumentException("Try executing action with null state");


        try {
            state.getSemaphore().acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // start vector clock based algorithm

        state.msgBag.add(message);

        logger.debug("message received = "+message);
        logger.debug("msgBag content = " + state.msgBag);

        for (ChatMessage msg : state.msgBag) {
            int q = msg.getSender();
            if((state.identity == q)||(state.clock.isPrecededByAndFIFO(msg.getClock(), msg.getSender()))) {

                deliver(message);
                state.nbChatMessageContentReceived++;

                if(q != state.identity) state.clock.incrementEntry(q);
            }
        }

        state.getSemaphore().release();
        return null;
    }

    /**
     * @param message the message to be displayed
     */
    private static void deliver(ChatMessage message) {
        logger.debug("Deliver "+message+" to console");
        System.out.println(message.getContent());
    }

    /**
     * Executed when the client receive an exit command
     *
     * @param msg the exit message received
     * @return null
     */
    public static Object receiveExitMessage(ExitMessage msg) {
        System.out.println("Bye. "+ (msg.hasMessage() ? "["+msg.getMessage()+"]" : "") );
        System.exit(0);
        return null;
    }
}
