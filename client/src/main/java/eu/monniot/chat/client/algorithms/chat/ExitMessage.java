package eu.monniot.chat.client.algorithms.chat;

import eu.monniot.chat.commons.AbstractMessage;

/**
 * Define an exit message (ie. a message that tell the client to close itself)
 */
public class ExitMessage extends AbstractMessage {
    private String message;

    public ExitMessage(String message) {
        this.message = message;
    }

    /**
     * @return an optional message to be displayed at the disconnection
     */
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ExitMessage{" +
                "message=" + message +
                '}';
    }

    /**
     * @return this command have a message to display
     */
    public boolean hasMessage() {
        return message != null && !message.isEmpty();
    }
}
