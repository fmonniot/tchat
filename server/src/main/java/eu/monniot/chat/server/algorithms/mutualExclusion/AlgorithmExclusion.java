package eu.monniot.chat.server.algorithms.mutualExclusion;

import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.commons.AlgorithmActionInterface;
import eu.monniot.chat.commons.AlgorithmActionInvocationException;
import eu.monniot.chat.commons.State;
import eu.monniot.chat.server.algorithms.ListOfAlgorithmsServer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum  AlgorithmExclusion implements AlgorithmActionInterface {
    RECEIVE_TOKEN(){
        @Override
        public Object execute(State state, AbstractMessage msg) throws AlgorithmActionInvocationException {
            if(!(msg instanceof TokenMessage))
                throw new AlgorithmActionInvocationException("message must be a TokenMessage.");

            if(!(state instanceof ExclusionServerState))
                throw new IllegalStateException("state must implements ElectionServerState.");

            return AlgorithmExclusionActions.receiveToken((ExclusionServerState)state, (TokenMessage)msg);
        }
    },
    RECEIVE_ASK(){
        @Override
        public Object execute(State state, AbstractMessage msg) throws AlgorithmActionInvocationException {
            if(!(msg instanceof AskMessage))
                throw new AlgorithmActionInvocationException("message must be a TokenMessage.");

            if(!(state instanceof ExclusionServerState))
                throw new IllegalStateException("state must implements ElectionServerState.");

            return AlgorithmExclusionActions.receiveAsk((ExclusionServerState)state, (AskMessage)msg);
        }
    };

    /**
     * unmodifiable collection corresponding to <tt>privateMapOfActions</tt>.
     */
    public final static Map<Integer, AlgorithmExclusion> mapOfActions;

    /**
     * collection of the actions of this algorithm. The Key is the index of the
     * corresponding message type. This collection is kept private to avoid
     * modifications.
     */
    private final static Map<Integer, AlgorithmExclusion> privateMapOfActions;

    /**
     * index of the first message type of this algorithm.
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final int algorithmOffset = 10;

    /**
     * index of the action of this message type.
     */
    private final int actionIndex;

    /**
     * static block to build collections of actions.
     */
    static {
        privateMapOfActions = new HashMap<>();
        for (AlgorithmExclusion algorithmElection : AlgorithmExclusion.values()) {
            privateMapOfActions.put(algorithmElection.actionIndex, algorithmElection);
        }
        mapOfActions = Collections.unmodifiableMap(privateMapOfActions);
    }

    /**
     * constructor of message type object.
     */
    private AlgorithmExclusion() {
        this.actionIndex = ListOfAlgorithmsServer.CHAT_SERVER_START_INDEX + algorithmOffset + ordinal();
    }

    /**
     * obtains the index of this message type.
     */
    @Override
    public int getActionIndex() {
        return actionIndex;
    }

    @Override
    public String toString() {
        return String.valueOf(actionIndex);
    }
}
