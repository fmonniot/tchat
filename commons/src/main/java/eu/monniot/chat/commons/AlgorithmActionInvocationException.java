package eu.monniot.chat.commons;

/**
 * This exception is thrown when the type of a message content just received
 * does not match to a message content type declared in the state machine
 * (client or server).
 *
 * @author Denis Conan
 */
public class AlgorithmActionInvocationException extends Exception {
    /**
     * Serial version Unique Identifier for this exception.
     */
    private static final long serialVersionUID = 1L;

    /**
     * @see Exception#Exception()
     */
    public AlgorithmActionInvocationException() {
        super();
    }

    /**
     * @param message the output to print
     *
     * @see Exception#Exception(java.lang.String)
     */
    public AlgorithmActionInvocationException(final String message) {
        super(message);
    }

    /**
     * Creates an exception caused by another exception.
     *
     * @param cause a throwable that cause this exception.
     *
     * @see Exception#Exception(java.lang.Throwable)
     */
    public AlgorithmActionInvocationException(final Throwable cause) {
        super();
    }

    /**
     * Creates an exception caused by another exception.
     *
     * @param message the detailed output.
     * @param cause   a throwable that cause this exception.
     *
     * @see Exception#Exception(java.lang.String, java.lang.Throwable)
     */
    public AlgorithmActionInvocationException(final String message, final Throwable cause) {
        super(message);
    }
}
