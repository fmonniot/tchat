package eu.monniot.chat.commons;


/**
 * This interface defines the interface of the actions of the state machines
 * (client or server).
 *
 * @author Denis Conan
 */
public interface AlgorithmActionInterface {
    int getActionIndex();

    Object execute(State state, AbstractMessage msg) throws AlgorithmActionInvocationException;
}
