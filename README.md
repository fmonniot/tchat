# README #

This project is a chat multi-server and multi-client build in Java.
There is a report (in french, sorry) in the ```docs``` folder containing
some information about the algorithm used.

### Build the executable

```sh
./gradlew installApp
```

### Command line ###

```client.sh <machine> <port>```

```server.sh <server number> <list of pairs hostname / server number>```

### Set up a sample infrastructure ###

Schema:

```txt
          Client0  Client1
           ||     //
           ||    //
           ||   //
           Server0 ======= Server1
               \\          //
                \\        //
                 \\      //
                  \\    //
                   Server2 ======= Server3
                     ||             ||   \\
                     ||             ||    \\
                     ||             ||     \\
                  Client2          Client3  Client4
```

Launch some server: 

1. ```server.sh 0```
2. ```server.sh 1 localhost 0```
3. ```server.sh 2 localhost 0 localhost 1```
4. ```server.sh 3 localhost 2```

And then the server:

1. ```client.sh localhost 2050```
2. ```client.sh localhost 2050```
3. ```client.sh localhost 2052```
4. ```client.sh localhost 2053```

### TODO Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### TODO Who do I talk to? ###

* Repo owner or admin
* Other community or team contact