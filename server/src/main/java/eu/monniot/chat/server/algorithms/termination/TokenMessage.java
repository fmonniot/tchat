package eu.monniot.chat.server.algorithms.termination;

import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.server.algorithms.ControlMessage;

/**
 * A token exchanged by the termination detection algorithm
 */
public class TokenMessage extends AbstractMessage implements ControlMessage {
    private int color;
    private int q;

    /**
     *
     * @param color the color of this token
     * @param q the transit message counter
     */
    public TokenMessage(int color, int q) {
        this.color = color;
        this.q = q;
    }

    /**
     * Either {@link eu.monniot.chat.server.algorithms.termination.TerminationServerState#WHITE} or
     * {@link eu.monniot.chat.server.algorithms.termination.TerminationServerState#BLACK}
     *
     * @return the color of this token
     */
    public int getColor() {
        return color;
    }

    /**
     * @return the transit message counter
     */
    public int getQ() {
        return q;
    }

    @Override
    public String toString() {
        return "TokenMessage{" +
                "color=" + color +
                ", q=" + q +
                '}';
    }
}
