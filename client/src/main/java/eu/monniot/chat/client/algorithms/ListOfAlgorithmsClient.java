package eu.monniot.chat.client.algorithms;

import eu.monniot.chat.client.algorithms.chat.AlgorithmChat;
import eu.monniot.chat.commons.*;

import java.util.Map;

/**
 * This Enumeration type declares the eu.monniot.chat.client.algorithms of the client's state machine.
 * For now, there is only one algorithm: the algorithm for exchanging chat messages.
 *
 * @author Denis Conan
 */
public enum ListOfAlgorithmsClient implements ListOfAlgorithms {
    /**
     * message types between clients exchanged to chat.
     */
    ALGORITHM_CHAT(AlgorithmChat.mapOfActions);

    /**
     * index of the first message type of the first algorithm of the client.
     */
    public final static int CHAT_CLIENT_START_INDEX = 1000;

    private final Map<Integer, ? extends AlgorithmActionInterface> actions;

    /**
     * constructor of this algorithm object.
     *
     * @param actions collection of actions of this algorithm.
     */
    private ListOfAlgorithmsClient(Map<Integer, ? extends AlgorithmActionInterface> actions) {
        this.actions = actions;
    }

    @Override
    public Map<Integer, ? extends AlgorithmActionInterface> getActions() {
        return actions;
    }

    /**
     * Try to execute the action with a client algorithm
     *
     * @see ListOfAlgorithmsDelegate#execute(State, int, Object, ListOfAlgorithms[])
     */
    public static void execute(final State state, final int actionIndex, final Object content)
            throws AlgorithmActionInvocationException {
        ListOfAlgorithmsDelegate.execute(state, actionIndex, content, ListOfAlgorithmsClient.values());
    }
}
