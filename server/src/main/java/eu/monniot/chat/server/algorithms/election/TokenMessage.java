package eu.monniot.chat.server.algorithms.election;

/**
 * Message of type token in the echo algorithm
 */
public class TokenMessage extends ElectionMessage {
    private static final long serialVersionUID = 1L;

    public TokenMessage(int sender, int initiator) {
        super(sender, initiator);
    }

    @Override
    public String toString() {
        return "TokenMessage{" +
                "sender=" + getSender() +
                ", initiator=" + getInitiator() +
                '}';
    }
}
