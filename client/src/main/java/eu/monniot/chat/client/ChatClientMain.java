package eu.monniot.chat.client;

import eu.monniot.chat.client.algorithms.chat.AlgorithmChat;
import eu.monniot.chat.client.algorithms.chat.ChatMessage;
import eu.monniot.chat.client.algorithms.chat.ExitMessage;
import eu.monniot.chat.commons.AbstractMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SocketChannel;

/**
 * This class is the main of the client chat. It configures the client, connects
 * to a chat server, launches a thread for reading chat messages from the
 * standard input, and enters an infinite loop for receiving messages and
 * executing the corresponding action of the client's state machine.
 *
 * @author chris
 * @author Denis Conan
 */
class ChatClientMain {

    private static final Logger logger = LogManager.getLogger();

    public static void main(final String[] args) {
        if (args.length != 2) {
            System.out.println("usage: java chatClientMain <machine> <port> ");
            return;
        }

        final boolean debug = System.getenv().containsKey("DEBUG");

        ChatClientState clientState = new ChatClientState();
        ChatClientThreadReadMessage myClient;
        BufferedReader reader;

        try {
            SocketChannel rwChan = SocketChannel.open();
            Socket rwCon = rwChan.socket();

            // We get the target machine's IP (domain, port)
            InetSocketAddress rcvAddress = new InetSocketAddress(
                    InetAddress.getByName(args[0]),
                    Integer.parseInt(args[1])
            );
            rwCon.connect(rcvAddress);

            myClient = new ChatClientThreadReadMessage(rwChan, clientState, debug);
        } catch (IOException ie) {
            ie.printStackTrace();
            return;
        }

        new Thread(myClient).start(); // this thread will listen to incoming messages

        // main program reads from stdin and sendMessages
        reader = new BufferedReader(new InputStreamReader(System.in));
        while (!clientState.exit) {
            String lineRead;
            long sent;
            AbstractMessage msg;
            // read a line and send the array dataBuf
            try {
                lineRead = reader.readLine();
                if (lineRead == null) break;

                // special command interpretation
                switch (lineRead) {
                    case "exit":
                        msg = new ExitMessage("");
                        sent = myClient.sendMsg(AlgorithmChat.EXIT_MESSAGE.getActionIndex(),
                                clientState.identity, 0, msg);
                        break;

                    default:
                        clientState.clock.incrementEntry(clientState.identity);
                        msg = new ChatMessage(clientState.identity, lineRead, clientState.clock);

                        // The sequence number is irrelevant (assigned to 0) for client messages sent to the server,
                        // but will be assigned by the server to control the propagation of client messages.
                        sent = myClient.sendMsg(AlgorithmChat.CHAT_MESSAGE.getActionIndex(),
                                clientState.identity, 0, msg);

                        clientState.nbChatMessageContentSent++;
                }
                logger.debug(sent + " bytes sent.");

            } catch (ClosedChannelException cce) {
                logger.info("Server closed the connection.");
                break;
            } catch (IOException ie) {
                ie.printStackTrace();
                break;
            }
        }

        try {
            myClient.close();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}
