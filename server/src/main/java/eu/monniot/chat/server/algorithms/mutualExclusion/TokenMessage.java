package eu.monniot.chat.server.algorithms.mutualExclusion;

import eu.monniot.chat.commons.AbstractMessage;
import eu.monniot.chat.commons.VectorClock;

/**
 * A token exchanged by the mutual exclusion algorithm (Ricart and Agalawa, 1983)
 */
public class TokenMessage  extends AbstractMessage {

    private VectorClock clock;

    public TokenMessage(VectorClock clock) {
        this.clock = clock;
    }

    /**
     * @return the table of this token
     */
    public VectorClock getClock() {
        return clock;
    }

    @Override
    public String toString() {
        return "TokenMessage{" +
                "clock=" + clock +
                '}';
    }
}
